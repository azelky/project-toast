import React from 'react';

import useKeydown from '../../hooks/use-keydown';

export const ToastContext = React.createContext({
  toasts: [],
  setToasts: () => {}
});

const VARIANT_OPTIONS = ['notice', 'warning', 'success', 'error'];

function ToastProvider({ children }) {
  const [toasts, setToasts] = React.useState([]);

  function createToast(variant, message) {
    const newToast = {
      id: new Date().valueOf(),
      variant,
      message
    };

    setToasts((previousToasts) => {
      return [...previousToasts, newToast];
    });
  }

  function dismissToast(id) {
    // Version 1
    const toastToDelete = toasts.find((toast) => toast.id === id);
    const toastIndex = toasts.indexOf(toastToDelete);
    const nextToasts = [...toasts];
    nextToasts.splice(toastIndex, 1);

    // Alternative
    // const nextToasts = toasts.filter((toast) => {
    //   return toast.id !== id;
    // });

    setToasts(nextToasts);
  }

  const handleEscape = React.useCallback(() => {
    setToasts([]);
  }, []);
  useKeydown('Escape', handleEscape);

  const value = {
    toasts,
    setToasts,
    variants: VARIANT_OPTIONS,
    createToast,
    dismissToast
  };

  return <ToastContext.Provider value={value}>{children}</ToastContext.Provider>;
}

export default ToastProvider;
