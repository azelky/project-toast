import { useEffect } from 'react';

function useKeydown(key, callback) {
  useEffect(() => {
    function handleKeydown(event) {
      if (event.code === key) {
        callback(event);
      }
    }

    document.addEventListener('keydown', handleKeydown);

    return () => {
      document.removeEventListener('keydown', handleKeydown);
    };
  }, [key, callback]);
}

export default useKeydown;
